package br.facens;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class StudentDAO {
    static final String DB_URL = "jdbc:mysql://localhost:3306/bd_students";
    static final String USER_NAME = "professor";
    static final String PASSWORD = "aula";

    // conectar ao BD
    // enviar uma instrução SQl para o BD
    // receber a resposta do BD
    // fechar a conexão

    public static boolean insert(Student student) {
        String sql = "insert into student (name, email) values (?, ?);";

        try (
                Connection connection = DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ) {
            preparedStatement.setString(1, student.getName());
            preparedStatement.setString(2, student.getEmail());
            preparedStatement.executeUpdate();

            return true;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static Student buscarPorID(int idStudent) {
        String sql = "Select * from student where id = ?;";
        try (
                Connection connection = DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);
                PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ) {
            preparedStatement.setInt(1, idStudent);
            ResultSet result = preparedStatement.executeQuery();
            if(result.next()) {
                int id = result.getInt("id");
                String nome = result.getString("name");
                String email = result.getString("email");

                Student student = new Student(id, nome, email);
                return student;
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public static boolean alterarEmail(int id, String novoEmail) {
        String sql = "Update student set email = ? where id = ?;";
        int registros = 0;

        try (
                Connection connection = DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);
                PreparedStatement preparedStatement = connection.prepareStatement(sql);            
        ) {
            preparedStatement.setString(1, novoEmail);
            preparedStatement.setInt(2, id);
            registros = preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return (registros > 0);
    }

    public static boolean apagarPorID(int id) {
        String sql = "Delete from student where id = ?;";
        int registros = 0;
        try (
                Connection connection = DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);
                PreparedStatement preparedStatement = connection.prepareStatement(sql);  
        ) {
            preparedStatement.setInt(1, id);
            registros = preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return (registros > 0);
    }
}
