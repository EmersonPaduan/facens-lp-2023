package exercicio01;

import java.util.Scanner;

/**
 * Exercicio01
 * 
 * 1. Faltas >= 16 = reprovado
 * 2. Média >= 6 = aprovado
 * 3. recalcular a media com a nota do exame
 * 4. Se novaMédia >=5 aprovado
 * 5. Reprovado
 */

public class Exercicio01 {

    public static void main(String[] args) {
        double nota1, nota2, media, exame;
        int faltas;
        boolean aprovado = false;
        Scanner entrada = new Scanner(System.in);

        // Entrada de dados
        System.out.println("Digite a primeira nota:");
        nota1 = entrada.nextDouble();

        System.out.println("Digite a segunda nota:");
        nota2 = entrada.nextDouble();

        System.out.println("Informe a quantidade de faltas:");
        faltas= entrada.nextInt();


        // Processamento
        if(faltas >= 16) {
            aprovado = false;
        } else {
            media = (nota1 + nota2) / 2;
            if(media >=6 ) {
                aprovado = true;
            } else {
                System.out.println("Informe a nota do exame:");
                exame = entrada.nextDouble();
                media = (media + exame) / 2;
                if(media >= 5) {
                    aprovado = true;
                }
            }
        }

        // Saída
        if(aprovado) {
            System.out.println("Aluno aprovado");
        } else {
            System.out.println("Aluno reprovado");
        }

        entrada.close();
    }
}