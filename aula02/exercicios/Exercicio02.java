package exercicios;

import java.util.Scanner;

/*
Escreva um programa que leia N números inteiros, até que o usuário digite 0 (zero)
 e exiba quantos são pares e quantos são ímpares. 
Dica: número par é divisível por 2 ( n % 2 == 0)

Exemplo:	

Digite o 1º número: 	4
Digite o 2º número: 	-201
.....

Digite o 10º número: 	0

O total de pares é: _____
O total de ímpares é: _____

*/

public class Exercicio02 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int numero, pares = 0, impares = 0;

        numero = 1;
        while (numero != 0) {
            System.out.println("Digite um valor inteiro (zero termina):");
            numero = entrada.nextInt();

            if(numero != 0) {
                if(numero % 2 == 0) {
                    pares++;
                } else {
                    impares++;
                }
            }
        }

        System.out.println("Quantidade de pares: " + pares);
        System.out.println("Quantidade de impares: " + impares);

        entrada.close();
    }
}
