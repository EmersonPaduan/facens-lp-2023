package exercicios;

import java.util.Scanner;

/*

Escreva um programa que exiba a tabuada do número digitado pelo usuário.

Exemplo: 

valor digitado: 5

5 x 0 =  0
5 x 1 =  5
5 x 2 = 10
...
5 x 10 = 50

*/

public class Exercicio01 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int numero;

        do {
            System.out.println("Digite o número da tabuada (1 a 10):");
            numero = entrada.nextInt();
        } while(numero < 1 || numero > 10);

        for (int i = 0; i <= 10; i++) {
            System.out.println(numero + " X " + i + " = " + (numero * i));
        }

        entrada.close();
    }
}
