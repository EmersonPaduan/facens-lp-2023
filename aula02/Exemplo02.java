
public class Exemplo02 {

    public static void main(String[] args) {
        int cont = 1;

        do {
            System.out.println(cont);
            cont++;
        } while (cont > 10);

        System.out.println("Fim do programa");
    }
}