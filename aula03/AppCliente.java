public class AppCliente {
    public static void main(String[] args) {
        // instanciando (criar) um objeto do tipo Cliente
        Cliente cliente1 = new Cliente("Carlos", 22);
        Cliente cliente2 = new Cliente();
        
        // cliente1.nome = "Carlos";
        // cliente1.idade = -200;
        cliente1.setIdade(-23); // não será aceito
        cliente1.setIdade(230); // não será aceito

        int idadeAtual = cliente1.getIdade();
        cliente1.setIdade(idadeAtual + 1);
        
        cliente1.apresentar();

        // cliente2.nome = "Tiago";
        cliente2.setNome("Tiago");
        cliente2.apresentar();

    }
}
