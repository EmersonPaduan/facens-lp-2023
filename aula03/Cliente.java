
public class Cliente {

    // atributos = características
    private String nome;
    private int idade;

    //método construtor default (padrão)
    public Cliente() {
        idade = 18;
    }

    //método construtor
    public Cliente(String nome, int idade) {
        this.nome = nome;
        this.idade = idade;
    }

    // métodos = ações
    public void apresentar() {
        System.out.println("Olá! Eu sou " + nome + " e tenho " + idade + " anos.");
    }

    public void setIdade(int novaIdade) {
        if(novaIdade > 0 && novaIdade <= 100) {
            idade = novaIdade;
        }
    }

    public int getIdade() {
        return idade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}